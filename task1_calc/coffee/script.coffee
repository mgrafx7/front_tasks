screenEl = document.getElementById('screen')
screenAdd = document.getElementById('screen-add')
str = ''
result = ''
number1 = 0
number2 = 0
fl = false
buf = 0
operator = ''
compute = false;

onNum = (num) ->
  number = undefined
  number = whatArgument(num)
  str += num
  str = number2  if compute
  setScreenAdd str
  setScreen number

whatArgument = (num) ->
  switch fl
    when true
      if operator in '*/'
        number2 = 0
      number1 = 0  if compute
      number2 *= 10
      number2 += num
      return number2
    when false
      number1 *= 10
      number1 += num
      return number1

onAdd = ->
  logicCalculator "+", 0

onSub = ->
  logicCalculator "-", 0

onMul = ->
  logicCalculator "*", 1

onDiv = ->
  logicCalculator "/", 1

logicCalculator = (operatorV, numberV) ->
  res operator
  setScreen number1
  operator = operatorV
  number2 = numberV
  fl = true
  compute = false
  str += operator
  setScreenAdd str

res = (operator) ->
  switch operator
    when '+'
      return number1 += number2
    when '-'
      return number1 -= number2
    when '*'
      if !fl
        checkNull number2
      return number1 *= number2
    when '/'
      if !fl
        checkNull number2
      return number1 /= number2

checkNull = ->
  if number2 == 0
    return number2 = 1

onCompute = ->
  res operator
  result = number1
  number2 = 0
  setScreen number1
  setScreenAdd ""
  compute = true
  str = number1
  fl = true

onReset = ->
  setScreen 0
  setScreenAdd ''
  str = ''
  result = 0
  number1 = 0
  number2 = 0
  compute = false
  fl = false

setScreen = (num) ->
  screenEl.innerHTML = num

setScreenAdd = (num) ->
  screenAdd.innerHTML = num